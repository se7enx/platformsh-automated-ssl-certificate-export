# Platform.sh automated SSL certificates export
This example shows how to setup automated SSL certificates export from your Platform.sh project.

## Installation
1. Install [acme.sh](https://github.com/Neilpang/acme.sh):
    - Download `acme.sh` script:
        ```bash
        cd %LOCAL_PROJECT_ROOT%
        wget https://raw.githubusercontent.com/Neilpang/acme.sh/master/acme.sh
        chmod +x acme.sh
        ```
    - Add mounts in `.platform.app.yaml`:
        ```yaml
        mounts:
            ...
            /.acme.sh:
                source: local
                source_path: acme-sh
            web/.well-known/acme-challenge:
                source: local
                source_path: acme-sh-challenge
        ```
    - Commit and push changes:
        ```bash
        git add acme.sh
        git add .platform.app.yaml
        git commit -m "Installing acme.sh"
        git push platform master
        ```
2. Install Platform.sh CLI:
    - Modify `build` hook in `.platform.app.yaml`:
        ```bash
        hooks:
            build: |
                set -e
                ...
                if [ ! -z "$PLATFORMSH_CLI_TOKEN" ]; then
                    curl -sS https://platform.sh/cli/installer | php
                fi
        ```
    - Set `PLATFORMSH_CLI_TOKEN` Platform.sh project variable (you can get its value [here](https://docs.platform.sh/gettingstarted/cli/api-tokens.html)):
        ```bash
        platform variable:create \
            --level=project \
            --name=PLATFORMSH_CLI_TOKEN \
            --value=%TOKEN_VALUE% \
            --json=false \
            --sensitive=true \
            --prefix=env \
            --visible-build=true \
            --visible-runtime=true
        ```
    - Commit and push changes:
        ```bash
        git add .platform.app.yaml
        git commit -m "Installing Platform.sh CLI"
        git push platform master
        ```
3. Setup cron job:
    - Create [`export_ssl_certificates.sh`](export_ssl_certificates.sh) bash script.
    - Add `export_ssl_certificates` cron in `.platform.app.yaml`:
        ```yaml
        crons:
            ...
            export_ssl_certificates:
                spec: '0 2 1,15 * *'
                cmd: |
                    if [ "$PLATFORM_BRANCH" = master ]; then
                        bash /app/export_ssl_certificates.sh
                    fi
        ```
    - Commit and push changes:
        ```bash
        git add export_ssl_certificates.sh
        git add .platform.app.yaml
        git commit -m "Export SSL Certificates cron"
        git push platform master
        ```